const http = require("http");
const fsp = require("fs").promises;
const { v4: uuidv4 } = require("uuid");

const server = http.createServer(async (request, response) => {
  try {
    let url = request.url;
    let statusCode = url.split("/")[2];
    if (url === "/html") {
      const htmlContent = await fsp.readFile("./src/index.html", "utf-8");
      response.writeHead(200, { "content-Type": "text/html" });
      response.end(htmlContent);
    } else if (url === "/json") {
      const jsonContent = await fsp.readFile("./src/data.json", "utf-8");
      response.writeHead(200, { "content-Type": "application/json" });
      response.end(jsonContent);
    } else if (url === "/uuid") {
      const jsonObj = { uuid: uuidv4() };
      response.writeHead(200, { "content-Type": "application/json" });
      response.end(JSON.stringify(jsonObj));
    } else if (url === `/status/${statusCode}`) {
      if (!http.STATUS_CODES[statusCode]) {
        response.end("Not valid status Code");
      } else {
        if (statusCode >= 100 && statusCode <= 103) {
          response.writeHead(200, { "content-Type": "text/html" });
        } else {
          response.writeHead(statusCode, { "content-Type": "text/html" });
        }
        response.write(
          `Response with status code ${statusCode}: ${http.STATUS_CODES[statusCode]}`
        );
        response.end();
      }
    } else if (url === `/delay/${statusCode}`) {
      if (isNaN(Number(statusCode)) || statusCode === "") {
        response.end("Time is not a number");
      } else {
        const delay = statusCode;
        response.writeHead(200, { "content-Type": "text/html" });
        setTimeout(() => {
          response.end(`Successful responce after ${delay} seconds.`);
        }, delay * 1000);
      }
    }
  } catch (e) {
    console.error(e);
  }
});

server.listen(8080, () => {
  console.log("Server listening on http://localhost:" + 8080);
});
